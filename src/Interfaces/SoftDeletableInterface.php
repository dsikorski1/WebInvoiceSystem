<?php

namespace App\Interfaces;

use Gedmo\SoftDeleteable\SoftDeleteable;

/**
 * Interface SoftDeletableInterface
 *
 * You should use this interface together with Gedmo\SoftDeleteable\Traits\SoftDeleteable
 *
 * @package App\Interfaces
 */
interface SoftDeletableInterface extends SoftDeleteable
{
    public function changeUniqueFields(): void;
    public function setDeletedAt(\DateTime $deletedAt = null);
    public function getDeletedAt();
    public function isDeleted();
}
