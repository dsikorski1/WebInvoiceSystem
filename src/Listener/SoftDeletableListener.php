<?php

namespace App\Listener;

use App\Interfaces\SoftDeletableInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;

class SoftDeletableListener
{
    public function preRemove(LifecycleEventArgs $args): void
    {
        $entity = $args->getEntity();

        if ($entity instanceof SoftDeletableInterface) {
            $entity->changeUniqueFields();
            $args->getEntityManager()->flush();
        }
    }
}
