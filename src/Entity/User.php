<?php

namespace App\Entity;

use App\Enum\RoleEnum;
use App\Interfaces\SoftDeletableInterface;
use Gedmo\SoftDeleteable\Traits\SoftDeleteable;
use Gedmo\Timestampable\Traits\Timestampable;
use Symfony\Component\Security\Core\User\UserInterface;

class User implements UserInterface, SoftDeletableInterface
{
    use Timestampable;
    use SoftDeleteable;

    /** @var string */
    private $id;

    /** @var string */
    private $username;

    /** @var string */
    private $email;

    /** @var string */
    private $password;

    /** @var string */
    private $salt;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function setUsername($username): void
    {
        $this->username = $username;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    public function getSalt(): string
    {
        return $this->salt;
    }

    public function setSalt(string $salt): void
    {
        $this->salt = $salt;
    }

    public function getRoles()
    {
        return [RoleEnum::ROLE_USER];
    }

    public function eraseCredentials(): void
    {
        return;
    }

    public function changeUniqueFields(): void
    {
        $this->username = $this->username . '_' . (new \DateTime())->getTimestamp();
        $this->email = $this->email . '_' . (new \DateTime())->getTimestamp();
    }
}
