<?php

namespace App\Enum;

class RoleEnum
{
    /** @var string  */
    const ROLE_USER = 'ROLE_USER';

    /** @var string  */
    const ROLE_ADMIN = 'ROLE_ADMIN';
}