<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class UserController extends Controller
{
    public function renderForm()
    {
        return $this->render('user/registration.html.twig');
    }
}
